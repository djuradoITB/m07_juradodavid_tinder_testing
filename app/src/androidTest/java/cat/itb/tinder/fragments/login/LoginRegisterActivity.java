package cat.itb.tinder.fragments.login;

import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.tinder.R;
import cat.itb.tinder.fragments.login.LoginActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class LoginRegisterActivity {

    @Rule
    public ActivityScenarioRule<LoginActivity> activityRule =
            new ActivityScenarioRule<>(LoginActivity.class);

//    First screen
    private ViewInteraction googleButton = onView(ViewMatchers.withId(R.id.login_google_button));
    private ViewInteraction facebookButton = onView(withId(R.id.login_facebook_button));
    private ViewInteraction phoneButton = onView(withId(R.id.login_phonenum_button));
    private ViewInteraction emailButton = onView(withId(R.id.textViewLoginEmail));

//    Second screen
    private ViewInteraction continueButton = onView(withId(R.id.button_continuar));

//    Third screen
    private ViewInteraction emailEditText = onView(withId(R.id.editTextEmail));
    private ViewInteraction emailSubmitButton = onView(withId(R.id.buttonRegisterContinuar));
    private String emailSample = "david.jurado.7e3@itb.cat";

    @Test
    public void testButtonsAreShown() {
        googleButton.check(matches(isDisplayed()));
        facebookButton.check(matches(isDisplayed()));
        phoneButton.check(matches(isDisplayed()));
        emailButton.check(matches(isDisplayed()));
    }

    @Test
    public void testLoginProcess() {
        emailButton.perform(click());

        continueButton.perform(click());

        emailEditText.perform(replaceText(emailSample));
        emailSubmitButton.perform(click());
    }

}

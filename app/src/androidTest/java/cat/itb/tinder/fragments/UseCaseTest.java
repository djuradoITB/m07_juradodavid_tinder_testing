package cat.itb.tinder.fragments;

import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.tinder.R;
import cat.itb.tinder.fragments.login.LoginActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class UseCaseTest {

    @Rule
    public ActivityScenarioRule<LoginActivity> activityRule =
            new ActivityScenarioRule<>(LoginActivity.class);

    private ViewInteraction emailButton = onView(withId(R.id.textViewLoginEmail));
    private ViewInteraction continueButton = onView(withId(R.id.button_continuar));
    private ViewInteraction emailEditText = onView(withId(R.id.editTextEmail));
    private ViewInteraction emailSubmitButton = onView(withId(R.id.buttonRegisterContinuar));
    private String emailSample = "david.jurado.7e3@itb.cat";

    @Test
    public void testLogin() {
            emailButton.perform(click());

            continueButton.perform(click());

            emailEditText.perform(replaceText(emailSample));
            emailSubmitButton.perform(click());
    }

}

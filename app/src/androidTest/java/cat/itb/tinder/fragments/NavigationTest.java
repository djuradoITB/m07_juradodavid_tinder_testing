package cat.itb.tinder.fragments;

import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.tinder.MainActivity;
import cat.itb.tinder.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.action.ViewActions.click;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class NavigationTest {

    private ViewInteraction topAppItemInici = onView(withId(R.id.itemFire));
    private ViewInteraction topAppItemChats = onView(withId(R.id.itemChats));
    private ViewInteraction topAppItemPerfil = onView(withId(R.id.itemProfile));

    @Rule
    public ActivityScenarioRule<MainActivity> activityRule =
            new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void testNavigationTopAppBar() {
        topAppItemInici.check(matches(isDisplayed()));
        topAppItemChats.check(matches(isDisplayed()));
        topAppItemPerfil.check(matches(isDisplayed()));

        /*
        Aquesta part del codi no funciona ja que em salta una excepció que diu que
        la vista ha de ser com a mínim 90% visible al usuari i no entenc
        a que es deu l'error. No ho he sapigut arreglar

        Documentació buscada:
        - https://stackoverflow.com/questions/29786413/performexception-error-performing-single-click
        - https://stackoverflow.com/questions/34510696/android-espresso-error-on-button-click
        - https://developer.android.com/training/testing/espresso/setup
        - https://developer.android.com/guide/navigation/navigation-testing

         */

        topAppItemChats.perform(click());
        topAppItemPerfil.perform(click());
        topAppItemInici.perform(click());
    }

}

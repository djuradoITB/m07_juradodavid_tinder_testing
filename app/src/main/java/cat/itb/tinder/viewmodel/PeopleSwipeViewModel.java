package cat.itb.tinder.viewmodel;

import android.widget.ImageView;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import cat.itb.tinder.R;
import cat.itb.tinder.model.ModelPerson;

public class PeopleSwipeViewModel extends ViewModel {

    private List<ModelPerson> shownPeople;

//    DEMO data
    public List<ModelPerson> getDemoData() {
        shownPeople = new ArrayList<>();

        ModelPerson mp = new ModelPerson(1, "David", 19, "Cis-Male", "Imagine this is a real biography", 4);
        mp.setSampleImages(new int[]{R.drawable.sample_photo_1, R.drawable.rosalia});
        shownPeople.add(mp);

        mp = new ModelPerson(2, "Juan", 19, "Cis-Male", "guaba nsque y esas cosas", 15);
        mp.setSampleImages(new int[]{R.drawable.sample_photo_2, R.drawable.rosalia});
        shownPeople.add(mp);

        mp = new ModelPerson(3, "Dani", 18, "Cis-Male", "Profesor i mascador de chicle profesional", 16);
        mp.setSampleImages(new int[]{R.drawable.sample_photo_3, R.drawable.rosalia});
        shownPeople.add(mp);

        mp = new ModelPerson(4, "Max", 22, "Cis-Male", "Texto de ejemplo", 8);
        mp.setSampleImages(new int[]{R.drawable.sample_photo_4, R.drawable.rosalia});
        shownPeople.add(mp);

        return shownPeople;
    }

}

package cat.itb.tinder.adapter;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

import cat.itb.tinder.model.ModelPerson;

public class PersonDiffCallback extends DiffUtil.Callback {

    private List<ModelPerson> old;
    private List<ModelPerson> newList;

    public PersonDiffCallback(List<ModelPerson> old, List<ModelPerson> newList) {
        this.old = old;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return old.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return old.get(oldItemPosition).getId() == newList.get(newItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return old.get(oldItemPosition) == newList.get(newItemPosition);
    }
}

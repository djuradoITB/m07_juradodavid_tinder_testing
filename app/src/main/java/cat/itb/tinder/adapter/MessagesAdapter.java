package cat.itb.tinder.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.tinder.R;
import cat.itb.tinder.model.ModelMessage;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessagesViewHolder> {

    private List<ModelMessage> modelMessageList;

    public MessagesAdapter(List<ModelMessage> modelMessageList) {
        this.modelMessageList = modelMessageList;
    }

    @NonNull
    @Override
    public MessagesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chats_messages_recycler_item, parent, false);

        return new MessagesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MessagesViewHolder holder, int position) {
        holder.bindData(modelMessageList.get(position));
    }

    @Override
    public int getItemCount() {
        return modelMessageList.size();
    }

    public class MessagesViewHolder extends RecyclerView.ViewHolder {

        private ImageView messagesImageView;
        private TextView messagesNameTextView;
        private TextView messagesLastMessageSentTextView;

        public MessagesViewHolder(@NonNull View itemView) {
            super(itemView);

            messagesImageView = itemView.findViewById(R.id.chats_messages_item_imageView);
            messagesNameTextView = itemView.findViewById(R.id.chats_messages_item_name_textView);
            messagesLastMessageSentTextView = itemView.findViewById(R.id.chats_messages_item_lastMessageSent_textView);
        }

        public void bindData(ModelMessage message) {
            messagesImageView.setImageResource(message.getMessagePhoto());
            messagesNameTextView.setText(message.getMessageName());
            messagesLastMessageSentTextView.setText(message.getLastMessageSent());
        }
    }

}

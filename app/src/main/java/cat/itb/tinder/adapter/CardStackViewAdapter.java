package cat.itb.tinder.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.List;

import cat.itb.tinder.R;
import cat.itb.tinder.model.ModelPerson;

public class CardStackViewAdapter extends RecyclerView.Adapter<CardStackViewAdapter.CardStackViewHolder> {

    private List<ModelPerson> personList;

    //    Variables
    private int imageHeight;
    private int imageWidth;
    private int posImage = 0;

    //    Constants
    final int NOT_PRESSED_CODE = -1;
    final int PRESSED_RIGHT_CODE = 0;
    final int PRESSED_LEFT_CODE = 1;
    final int PRESSED_DOWN_CODE = 2;

    public CardStackViewAdapter(List<ModelPerson> personList) {
        this.personList = personList;
    }

    @NonNull
    @Override
    public CardStackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_person_cardstack, parent, false);

        return new CardStackViewHolder(v);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull final CardStackViewHolder holder, int position) {
        final ModelPerson mp = personList.get(position);
        posImage = 0;

        holder.image.setImageResource(mp.getSampleImages()[posImage]);
        holder.name.setText(mp.getName());
        holder.age.setText(String.valueOf(mp.getAge()));
        holder.bio.setText(mp.getBiography());

        holder.updateImageSize();
        holder.image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != MotionEvent.ACTION_MOVE) {
                    float x = event.getX();
                    float y = event.getY();

                    switch (getClickEvent(x, y)) {
                        case PRESSED_DOWN_CODE:
                            break;
                        case PRESSED_LEFT_CODE:
                            if (posImage - 1 >= 0) {
                                posImage--;
                                holder.image.setImageResource(mp.getSampleImages()[posImage]);
                            }
                            break;
                        case PRESSED_RIGHT_CODE:
                            if (posImage + 1 < mp.getSampleImages().length) {
                                posImage++;
                                holder.image.setImageResource(mp.getSampleImages()[posImage]);
                            }
                            break;
                        default:
                    }
                }
                return false;
            }
        });

//        Carregar les imatges a l'ImageView traves d'una URL
        /*Glide.with(holder.image)
                .load(mp.getImages().get(position))
                .into(holder.image);*/
    }

    public List<ModelPerson> getPersonList() {
        return personList;
    }

    public void setPersonList(List<ModelPerson> personList) {
        this.personList = personList;
    }

    @Override
    public int getItemCount() {
        return personList.size();
    }

    public class CardStackViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView name;
        private TextView age;
        private TextView bio;

        public CardStackViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.item_person_cardstack_image);
            name = itemView.findViewById(R.id.item_person_cardstack_name);
            age = itemView.findViewById(R.id.item_person_cardstack_age);
            bio = itemView.findViewById(R.id.item_person_cardstack_bio);
        }

        private void updateImageSize() {
            imageHeight = image.getHeight();
            imageWidth = image.getWidth();
        }

    }

    private int getClickEvent(float x, float y) {
        final double PERCENT_HEIGHT = 0.2;
        final double PERCENT_WIDTH = 0.49;

        int pressed;
        int yDown = (int) (imageHeight - (imageHeight * PERCENT_HEIGHT));
        int xLeft = (int) (imageWidth * PERCENT_WIDTH);
        int xRight = (int) (imageWidth - (imageWidth * PERCENT_WIDTH));

        if (y >= yDown) {
            pressed = PRESSED_DOWN_CODE;
        }
        else {
            if (x <= xLeft) {
                pressed = PRESSED_LEFT_CODE;
            }
            else if (x >= xRight) {
                pressed = PRESSED_RIGHT_CODE;
            }
            else {
                pressed = NOT_PRESSED_CODE;
            }
        }

        return pressed;
    }

}

package cat.itb.tinder.adapter;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.tinder.R;
import cat.itb.tinder.model.ModelNewMatch;

public class NewMatchesAdapter extends RecyclerView.Adapter<NewMatchesAdapter.NewMatchesViewHolder>{

    private List<ModelNewMatch> modelNewMatchList;

    public NewMatchesAdapter(List<ModelNewMatch> modelNewMatchList) {
        this.modelNewMatchList = modelNewMatchList;
    }

    @NonNull
    @Override
    public NewMatchesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chats_newmatches_recycler_item, parent, false);

        return new NewMatchesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NewMatchesViewHolder holder, int position) {
        holder.bindData(modelNewMatchList.get(position));
    }

    @Override
    public int getItemCount() {
        return modelNewMatchList.size();
    }

    public class NewMatchesViewHolder extends RecyclerView.ViewHolder {

        private ImageView newMatchesImageView;
        private TextView newMatchesTextView;

        public NewMatchesViewHolder(@NonNull View itemView) {
            super(itemView);

            newMatchesImageView = itemView.findViewById(R.id.chats_newMatches_item_imageView);
            newMatchesTextView = itemView.findViewById(R.id.chats_newMatches_item_textView);
        }

        public void bindData(ModelNewMatch newMatch) {
            newMatchesImageView.setImageResource(newMatch.getNewMatchPhoto());
            newMatchesTextView.setText(newMatch.getNewMatchName());
        }
    }

}

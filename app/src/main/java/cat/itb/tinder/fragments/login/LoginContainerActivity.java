package cat.itb.tinder.fragments.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import cat.itb.tinder.R;
import cat.itb.tinder.fragments.login.LoginPhone;

public class LoginContainerActivity extends AppCompatActivity {
    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_container);

        currentFragment = new LoginPhone();
        changeFragment(currentFragment);
    }

    private void changeFragment(Fragment currentFragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.login_fragment_container, currentFragment).commit();
    }
}
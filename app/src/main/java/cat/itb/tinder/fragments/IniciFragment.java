package cat.itb.tinder.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.Direction;
import com.yuyakaido.android.cardstackview.Duration;
import com.yuyakaido.android.cardstackview.RewindAnimationSetting;
import com.yuyakaido.android.cardstackview.StackFrom;
import com.yuyakaido.android.cardstackview.SwipeAnimationSetting;
import com.yuyakaido.android.cardstackview.SwipeableMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cat.itb.tinder.R;
import cat.itb.tinder.adapter.CardStackViewAdapter;
import cat.itb.tinder.adapter.PersonDiffCallback;
import cat.itb.tinder.model.ModelPerson;
import cat.itb.tinder.viewmodel.PeopleSwipeViewModel;

public class IniciFragment extends Fragment {

//    Objects
    private CardStackLayoutManager manager;
    private CardStackViewAdapter adapter;
    private PeopleSwipeViewModel psvw;

//    Layout Objects
    public static CardStackView currentPersonCardStack;
    public FloatingActionButton buttonRewind;
    public FloatingActionButton buttonNo;
    public FloatingActionButton buttonSuperLike;
    public FloatingActionButton buttonLike;
    public FloatingActionButton buttonBoost;

    public IniciFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_inici, container, false);

        currentPersonCardStack = v.findViewById(R.id.fragment_inici_cardstack);
        buttonRewind = v.findViewById(R.id.button_rewind);
        buttonNo = v.findViewById(R.id.button_no);
        buttonSuperLike = v.findViewById(R.id.button_superlike);
        buttonLike = v.findViewById(R.id.button_like);
        buttonBoost = v.findViewById(R.id.button_boost);

        psvw = new PeopleSwipeViewModel();
        manager = new CardStackLayoutManager(getContext(), new CardListener());
        adapter = new CardStackViewAdapter(psvw.getDemoData()); //demo data

        initializeManager();
        setupButtons();

        return v;
    }

    private void initializeManager() {
        manager.setStackFrom(StackFrom.None);
        manager.setVisibleCount(2);
        manager.setTranslationInterval(0f);
        manager.setScaleInterval(1.0f);
        manager.setSwipeThreshold(0.35f);
        manager.setMaxDegree(35.0f);
        List<Direction> directions = Arrays.asList(Direction.Left, Direction.Right, Direction.Top);
        manager.setDirections(directions);
        manager.setCanScrollHorizontal(true);
        manager.setCanScrollVertical(true);
        manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual);
        manager.setOverlayInterpolator(new LinearInterpolator());

        currentPersonCardStack.setLayoutManager(manager);
        currentPersonCardStack.setAdapter(adapter);
    }

    private void setupButtons() {
        buttonRewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 RewindAnimationSetting settings = new RewindAnimationSetting.Builder()
                         .setDirection(Direction.Bottom)
                         .setDuration(Duration.Normal.duration)
                         .setInterpolator(new DecelerateInterpolator())
                         .build();
                 manager.setRewindAnimationSetting(settings);
                 currentPersonCardStack.rewind();
            }
        });

        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Left)
                        .setDuration(Duration.Normal.duration)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                manager.setSwipeAnimationSetting(setting);
                currentPersonCardStack.swipe();
            }
        });

        buttonLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Right)
                        .setDuration(Duration.Normal.duration)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                manager.setSwipeAnimationSetting(setting);
                currentPersonCardStack.swipe();
            }
        });

        buttonSuperLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Top)
                        .setDuration(Duration.Normal.duration)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                manager.setSwipeAnimationSetting(setting);
                currentPersonCardStack.swipe();
            }
        });

        buttonBoost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setTitle("A pagar")
                        .setMessage("Toca pagar si vols aquest botó")
                        .setNeutralButton("No vull pagar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
            }
        });
    }

    private void paginate() {
        List<ModelPerson> old = adapter.getPersonList();
        List<ModelPerson> newList = new ArrayList<>(old);
        newList.addAll(psvw.getDemoData());

        PersonDiffCallback callback = new PersonDiffCallback(old, newList);
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(callback);

        adapter.setPersonList(newList);
        result.dispatchUpdatesTo(adapter);
    }

    public class CardListener implements CardStackListener {

        @Override
        public void onCardDragging(Direction direction, float ratio) {
            Log.d("CardStackView", "onCardDragging: d = " + direction.name() + ", r = " + ratio);
        }

        @Override
        public void onCardSwiped(Direction direction) {
            Log.d("CardStackView", "onCardSwiped: p = " + manager.getTopPosition() + ", r = " + direction);
            if (manager.getTopPosition() == adapter.getItemCount() - 5) {
                paginate();
            }
        }

        @Override
        public void onCardRewound() {
            Log.d("CardStackView", "onCardRewound: " + manager.getTopPosition());
        }

        @Override
        public void onCardCanceled() {
            Log.d("CardStackView", "onCardCanceled: " + manager.getTopPosition());
        }

        @Override
        public void onCardAppeared(View view, int position) {
            TextView textView = view.findViewById(R.id.item_person_cardstack_name);
            Log.d("CardStackView", "onCardAppeared: " + position + textView.getText());
        }

        @Override
        public void onCardDisappeared(View view, int position) {
            TextView textView = view.findViewById(R.id.item_person_cardstack_name);
            Log.d("CardStackView", "onCardDisappeared: " + position + textView.getText());
        }
    }
}
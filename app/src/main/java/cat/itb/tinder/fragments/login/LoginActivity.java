package cat.itb.tinder.fragments.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cat.itb.tinder.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button gButt;
    private Button fButt;
    private Button pButt;

    TextView emailLoginText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        gButt = findViewById(R.id.login_google_button);
        fButt = findViewById(R.id.login_facebook_button);
        pButt = findViewById(R.id.login_phonenum_button);

        emailLoginText = findViewById(R.id.textViewLoginEmail);


        gButt.setOnClickListener(this);
        fButt.setOnClickListener(this);
        pButt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(LoginActivity.this, LoginContainerActivity.class);
        startActivity(i);
    }

}
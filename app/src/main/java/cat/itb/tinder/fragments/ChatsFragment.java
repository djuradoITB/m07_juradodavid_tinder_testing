package cat.itb.tinder.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cat.itb.tinder.R;
import cat.itb.tinder.adapter.MessagesAdapter;
import cat.itb.tinder.adapter.NewMatchesAdapter;
import cat.itb.tinder.linearLayoutManager.CustomLinearLayoutManager;
import cat.itb.tinder.model.ModelMessage;
import cat.itb.tinder.model.ModelNewMatch;

public class ChatsFragment extends Fragment {

    private RecyclerView hNewMatchesRecyclerView;
    private RecyclerView vMessagesRecyclerView;

    public ChatsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_chats, container, false);

//        Sample lists
        List<ModelNewMatch> newMatchList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ModelNewMatch nm = new ModelNewMatch(R.drawable.sample_newmatch_image, "David");
            newMatchList.add(nm);
        }
        List<ModelMessage> messageList = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            ModelMessage m = new ModelMessage(R.drawable.sample_message_image, "Juan", "Ey guape ;)");
            messageList.add(m);
        }

//        RecyclerView
        hNewMatchesRecyclerView = v.findViewById(R.id.chats_newMatches_h_recyclerView);
        vMessagesRecyclerView = v.findViewById(R.id.chats_messages_v_recyclerView);

        LinearLayoutManager newMatchesLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        hNewMatchesRecyclerView.setLayoutManager(newMatchesLayoutManager);

        CustomLinearLayoutManager messagesLayoutManager = new CustomLinearLayoutManager(getContext()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        vMessagesRecyclerView.setLayoutManager(messagesLayoutManager);

//        Adapter
        NewMatchesAdapter newMatchesAdapter = new NewMatchesAdapter(newMatchList);
        hNewMatchesRecyclerView.setAdapter(newMatchesAdapter);

        MessagesAdapter messagesAdapter = new MessagesAdapter(messageList);
        vMessagesRecyclerView.setAdapter(messagesAdapter);

        return v;
    }


}
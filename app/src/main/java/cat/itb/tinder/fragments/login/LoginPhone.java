package cat.itb.tinder.fragments.login;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import cat.itb.tinder.R;

public class LoginPhone extends Fragment {
    private Fragment currentFragment;
    private Button button;

    public LoginPhone() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login_phone, container, false);

        button = v.findViewById(R.id.button_continuar);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentFragment = new RegisterCodeFragment();
                changeFragment(currentFragment);
            }
        });

        return v;
    }

    private void changeFragment(Fragment currentFragment) {
        getFragmentManager().beginTransaction().replace(R.id.login_fragment_container, currentFragment).commit();
    }

}
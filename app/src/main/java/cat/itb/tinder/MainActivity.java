package cat.itb.tinder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import cat.itb.tinder.fragments.ChatsFragment;
import cat.itb.tinder.fragments.IniciFragment;
import cat.itb.tinder.fragments.ProfileFragment;

public class MainActivity extends AppCompatActivity {

    private Fragment currentFragment;
    private BottomNavigationView bottomNavigationView;
    private BadgeDrawable badgeDrawable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_main);


//        Initialize screen
        currentFragment = new IniciFragment();
        changeFragment(currentFragment);
        bottomNavigationView = findViewById(R.id.topAppNavigation);

//        Add menu badge
        badgeDrawable = bottomNavigationView.getOrCreateBadge(R.id.itemLikes);
        badgeDrawable.setNumber(99);
        badgeDrawable.setVisible(true);

//        Navigation Listener
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.itemFire:
                        currentFragment = new IniciFragment();
                        break;
                    case R.id.itemLikes:
                        break;
                    case R.id.itemChats:
                        currentFragment = new ChatsFragment();
                        break;
                    case R.id.itemProfile:
                        currentFragment = new ProfileFragment();
                        break;
                }
                changeFragment(currentFragment);
                return false;
            }
        });
    }


    private void changeFragment(Fragment currentFragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.mainFrameLayout, currentFragment).commit();
    }
}
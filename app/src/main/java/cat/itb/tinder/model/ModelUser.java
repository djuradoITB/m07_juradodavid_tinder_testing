package cat.itb.tinder.model;

public class ModelUser {

    public String email;

    public ModelUser(){}

    public ModelUser(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

package cat.itb.tinder.model;

import android.widget.ImageView;
import android.widget.TextView;

public class ModelNewMatch {

    private int newMatchPhoto;
    private String newMatchName;

    public ModelNewMatch(int newMatchPhoto, String newMatchName) {
        this.newMatchPhoto = newMatchPhoto;
        this.newMatchName = newMatchName;
    }

    public int getNewMatchPhoto() {
        return newMatchPhoto;
    }

    public String getNewMatchName() {
        return newMatchName;
    }

}

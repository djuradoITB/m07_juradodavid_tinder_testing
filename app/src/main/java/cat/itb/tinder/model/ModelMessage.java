package cat.itb.tinder.model;

public class ModelMessage {

    private int messagePhoto;
    private String messageName;
    private String lastMessageSent;

    public ModelMessage(int messagePhoto, String messageName, String lastMessageSent) {
        this.messagePhoto = messagePhoto;
        this.messageName = messageName;
        this.lastMessageSent = lastMessageSent;
    }

    public int getMessagePhoto() {
        return messagePhoto;
    }

    public String getMessageName() {
        return messageName;
    }

    public String getLastMessageSent() {
        return lastMessageSent;
    }
}

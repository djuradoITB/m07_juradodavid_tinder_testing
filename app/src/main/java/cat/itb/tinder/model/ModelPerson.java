package cat.itb.tinder.model;

import java.util.List;

public class ModelPerson {

    private int id;
    private String name;
    private int age;
    private String gender;
    private String biography;
    private int location;
    private List<String> images;
    private int[] sampleImages;
    private String genderPreferences;
    private int minAge;
    private int maxAge;
    private int minDistance;
    private int maxDistance;
    private String phoneNumber;

    public ModelPerson(int id, String name, int age, String gender, String biography, int location, List<String> images) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.biography = biography;
        this.location = location;
        this.images = images;
    }

//    DEMO constructor
    public ModelPerson(int id, String name, int age, String gender, String biography, int location) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.biography = biography;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getGenderPreferences() {
        return genderPreferences;
    }

    public void setGenderPreferences(String genderPreferences) {
        this.genderPreferences = genderPreferences;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public int getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(int minDistance) {
        this.minDistance = minDistance;
    }

    public int getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int[] getSampleImages() {
        return sampleImages;
    }

    public void setSampleImages(int[] sampleImages) {
        this.sampleImages = sampleImages;
    }
}
